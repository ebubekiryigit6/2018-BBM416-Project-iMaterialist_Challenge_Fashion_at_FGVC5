import numpy as np
import torch.utils.data as data
from PIL import Image
import os
import os.path

import dataset_downloader

"""

We use torchvision/datasets/folder.py ImageFolder class
But it uses parent of "train" and "test" image folders.
Our dataset is mixed. Train and test images in same folder.
By making changes in this class I made the ImageFolder initializer able to read our dataset. 
After making the required edits, a pull request will be sent to GitHub.

"""


def getClassList(json_path):
    """
    finds all classes in given dataset and indexes 0 to len class list

    :return: returns a tuple of class list and class index (class list , class index dict)
    """
    classes = []
    train_json_loader = dataset_downloader.JsonLoader(json_path)
    train_json = train_json_loader.get_data()
    annotations = train_json['annotations']
    for annotation in annotations:
        labelIds = annotation['labelId']
        # imageId = annotation['imageId']
        for i in labelIds:
            if i not in classes:
                classes.append(i)
    classes.sort()
    return classes, train_json_loader.get_annotations()


def pil_loader(path):
    # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
    with open(path, 'rb') as f:
        with Image.open(f) as img:
            return img.convert('RGB')


def default_loader(path):
    # open as rgb image
    return pil_loader(path)


def default_flist_reader(data_path, labels_dict, classes):
    imlist = []

    for img in os.listdir(data_path):
        imageId = img.split(".")[0]
        label_list = np.zeros(len(classes), dtype=float)
        img_labels = labels_dict[imageId]
        for i in img_labels:
            label_list[classes.index(i)] = 1
        imlist.append((os.path.join(data_path, img), label_list))
    return imlist

    # with open(flist, 'r') as rf:
    #     for line in rf.readlines():
    #         line = line.strip().strip("\n").strip()
    #         if line:
    #             impath = line.strip()
    #             imlabel = impath.split("/")[0]
    #             imlist.append((impath, class_to_idx[imlabel]))
    #
    # return imlist


class ImageFilelist(data.Dataset):

    def __init__(self, data_path, json_path, transform=None, target_transform=None,
                 flist_reader=default_flist_reader, loader=default_loader):

        classes, labels_dict = getClassList(json_path)

        self.classes = classes
        self.data_path = data_path
        self.imlist = flist_reader(data_path, labels_dict, classes)
        self.transform = transform
        self.target_transform = target_transform
        self.loader = loader

    def __getitem__(self, index):
        impath, target = self.imlist[index]
        img = self.loader(impath)
        if self.transform is not None:
            img = self.transform(img)
        if self.target_transform is not None:
            target = self.target_transform(target)

        return img, target

    def __len__(self):
        return len(self.imlist)
