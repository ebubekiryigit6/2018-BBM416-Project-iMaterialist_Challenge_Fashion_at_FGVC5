import sys
import train_model


def main(argv):
    pickle_path = argv[1]
    csv_path = argv[2]

    image_to_label = train_model.predict_model(pickle_path)
    train_model.write_predicted_data_to_csv(image_to_label, csv_path)
    return


if __name__ == "__main__":
    main(sys.argv)
    # pass
