import sys
import time

import constants
import train_model
import util
import dataset_downloader


def main(argv):
    options = util.parseArguments(argv)
    if not constants.TEMP_ENABLED:
        dataset_downloader.download_dataset_if_is_necessary()
    train_model.startTrainingWithDataLoaders(options)


if __name__ == "__main__":
    util.writeLog("MAIN-PROCESS", "Image Classification of Fashion Products is started.")
    start = time.time()
    main(sys.argv)
    util.writeLog("MAIN-PROCESS",
                  "Image Classification of Fashion Products is finished in "
                  + str(time.time() - start) + " seconds.")
