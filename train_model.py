import collections
import copy
import pickle

import os

from PIL import Image
from torch.optim import lr_scheduler
import torch
from torch.autograd import Variable
from torchvision import transforms, models
import time
from torch import nn
import constants
import util
import image_loader as imloader
from sklearn.metrics import f1_score


def train_model(data_loaders, data_sizes, model, criterion, optimizer, scheduler, num_epochs):
    util.writeLog("TRAINING", "Train is started with"
                  + "\nTrain Size: " + str(data_sizes[constants.TRAIN_DATA_LABEL])
                  + "\nTest Size:  " + str(data_sizes[constants.TEST_DATA_LABEL]))

    since = time.time()

    best_acc = 0.0

    # data for each epochs"
    trainLoss = []
    trainCorrect = []
    testLoss = []
    testCorrect = []

    best_model_wts = copy.deepcopy(model.state_dict())

    for epoch in range(num_epochs):
        util.writeLog("EPOCH-LOGGER", 'Epoch {}/{}'.format(epoch, num_epochs - 1))

        # Each epoch has a training and validation phase
        for phase in [constants.TRAIN_DATA_LABEL, constants.TEST_DATA_LABEL]:
            if phase == constants.TRAIN_DATA_LABEL:
                scheduler.step()
                model.train(True)  # Set model to training mode
            else:
                model.train(False)  # Set model to evaluate mode

            running_loss = 0.0
            f1_scores = 0.0

            # Iterate over data.
            for data in data_loaders[phase]:
                # get the inputs
                inputs, labels = data

                # wrap them in Variable
                if constants.USE_GPU:
                    inputs = Variable(inputs.cuda())
                    labels = Variable(labels.cuda())
                else:
                    inputs = Variable(inputs)
                    labels = Variable(labels).float()

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                outputs = model(inputs)
                m = nn.Sigmoid()
                preds = m(outputs) > 0.5

                loss = criterion(outputs, labels)

                # backward + optimize only if in training phase
                if phase == constants.TRAIN_DATA_LABEL:
                    loss.backward()
                    optimizer.step()

                # statistics
                f1_scores += f1_score(preds.data, labels.data, average='samples')
                running_loss += loss.data[0] * inputs.size(0)
                # running_corrects += torch.sum(preds == labels.data)

            epoch_loss = running_loss / data_sizes[phase]
            epoch_acc = f1_scores

            util.writeLog("F1-Score", 'Epoch {}/{}'.format(epoch, num_epochs - 1) + " Score: " + str(epoch_acc))
            util.writeLog("Loss-Value", 'Epoch {}/{}'.format(epoch, num_epochs - 1) + " Loss: " + str(epoch_loss))

            if phase == constants.TEST_DATA_LABEL and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())

            if phase == constants.TRAIN_DATA_LABEL:
                trainCorrect.append(epoch_acc)
                trainLoss.append(epoch_loss)
            else:
                testCorrect.append(epoch_acc)
                testLoss.append(epoch_loss)

    time_elapsed = time.time() - since

    util.writeLog("DATA-LOGGER", "Train Top1 Acc: " + str(trainCorrect))
    util.writeLog("DATA-LOGGER", "Train Loss: " + str(trainLoss))

    util.writeLog("DATA-LOGGER", "Test Top1 Acc: " + str(testCorrect))
    util.writeLog("DATA-LOGGER", "Test Loss: " + str(testLoss))

    util.writeLog("TRAINING", 'Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    util.writeLog("TRAINING", 'Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model


def getTrainValConfigs():
    train_config = imloader.ImageFilelist(data_path=constants.TRAIN_IMAGES_PATH,
                                          json_path=constants.TRAIN_URLS_JSON_FILE,
                                          transform=transforms.Compose([
                                              transforms.Resize(256),
                                              transforms.CenterCrop(224),
                                              transforms.ToTensor(),
                                              transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                                          ]))

    val_config = imloader.ImageFilelist(data_path=constants.VAL_IMAGES_PATH,
                                        json_path=constants.TRAIN_URLS_JSON_FILE,
                                        transform=transforms.Compose([
                                            transforms.Resize(256),
                                            transforms.CenterCrop(224),
                                            transforms.ToTensor(),
                                            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                                        ]))

    return train_config, val_config


def resnet(class_names, freeze_data_num, learning_rate, which):
    if which == 18:
        model_ft = models.resnet18(pretrained=True)
    elif which == 34:
        model_ft = models.resnet34(pretrained=True)
    elif which == 50:
        model_ft = models.resnet50(pretrained=True)
    elif which == 101:
        model_ft = models.resnet101(pretrained=True)
    else:
        model_ft = models.resnet152(pretrained=True)

    # 3 set of 18 (We can freeze 0 to 54 layers)
    # we choose 48 (freeze 16 sets)
    counter = 0
    for param in model_ft.parameters():
        param.requires_grad = False
        counter += 1
        if counter == freeze_data_num:
            break

    num_ftrs = model_ft.fc.in_features
    model_ft.fc = torch.nn.Linear(num_ftrs, len(class_names))

    if constants.USE_GPU:
        model_ft = model_ft.cuda()

    criterion = torch.nn.MultiLabelSoftMarginLoss()

    # Observe that all parameters are being optimized
    optimizer_ft = torch.optim.Adam(filter(lambda p: p.requires_grad, model_ft.parameters()),
                                    lr=learning_rate)

    # Decay LR by a factor of 0.1 every 7 epochs
    exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)

    return model_ft, criterion, optimizer_ft, exp_lr_scheduler


def predict_model(pickle_path):
    classes, _ = imloader.getClassList(constants.TRAIN_URLS_JSON_FILE)

    scaler = transforms.Resize((224, 224))
    normalizer = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    to_tensorer = transforms.ToTensor()

    with open(pickle_path, 'rb') as handle:
        model = pickle.load(handle)
    model.eval()

    image_to_labels_dict = {}

    for im in os.listdir(constants.TEST_IMAGES_PATH):
        img_full_path = os.path.join(constants.TEST_IMAGES_PATH, im)
        img = Image.open(img_full_path).convert('RGB')
        t_img = Variable(normalizer(to_tensorer(scaler(img))).unsqueeze(0))
        outputs = model(t_img)
        m = nn.Sigmoid()
        preds = m(outputs) > 0.5
        # label_indexes = np.argwhere(np.array(preds.data, dtype=int) == 1).flatten()
        labels = []
        for i in range(len(preds[0].data)):
            have = preds[0].data[i]
            if have == 1:
                labels.append(classes[i])
        image_to_labels_dict[int(im.split(".")[0])] = labels
    image_to_labels_dict = collections.OrderedDict(sorted(image_to_labels_dict.items()))
    return image_to_labels_dict


def write_predicted_data_to_csv(image_to_labels_dict, csv_path):
    csv_file = open(csv_path, "w")
    csv_file.write("image,label\n")
    for key, value in image_to_labels_dict.items():
        row = str(key) + "," + " ".join(value) + "\n"
        csv_file.write(row)
    csv_file.close()


def startTrainingWithDataLoaders(options):
    # get the option args
    num_of_epochs = int(options.num_epoch)
    batch_size = int(options.batch_size)
    worker_num = int(options.worker_num)
    learning_rate = float(options.learning_rate)
    freeze_data_num = int(options.freeze_data_num)
    model_to_use = options.model_to_use

    util.writeLog("ARG-PARSER", "Argument Informations:"
                  + "\nMODEL: " + options.model_to_use
                  + "\nEPOCH: " + options.num_epoch
                  + "\nBATCH SIZE: " + options.batch_size
                  + "\nWORKERS: " + options.worker_num
                  + "\nLEARNING RATE: " + str(options.learning_rate)
                  + "\nFREEZE LAYER NUM: " + str(options.freeze_data_num))

    util.writeLog("GPU-ENGINE", "GPU Enabled: " + str(constants.USE_GPU))
    util.writeLog("MODEL-FREEZER", "Freeze Model Parameters: " + str(freeze_data_num))

    train_config, val_config = getTrainValConfigs()
    train_loader = torch.utils.data.DataLoader(train_config,
                                               batch_size=batch_size, shuffle=True,
                                               num_workers=worker_num)
    val_loader = torch.utils.data.DataLoader(val_config,
                                             batch_size=batch_size, shuffle=True,
                                             num_workers=worker_num)
    util.writeLog("DATA-LOADER", "Train and Test Data has been loaded.")
    data_loaders = {constants.TRAIN_DATA_LABEL: train_loader,
                    constants.TEST_DATA_LABEL: val_loader}
    data_sizes = {constants.TRAIN_DATA_LABEL: len(train_config),
                  constants.TEST_DATA_LABEL: len(val_config)}

    # get class lists
    class_names = train_config.classes

    if model_to_use.lower() == constants.RESNET50.lower():
        model_ft, criterion, optimizer_ft, exp_lr_scheduler = resnet(class_names, freeze_data_num, learning_rate, 50)
    elif model_to_use.lower() == constants.RESNET152.lower():
        model_ft, criterion, optimizer_ft, exp_lr_scheduler = resnet(class_names, freeze_data_num, learning_rate, 152)
    elif model_to_use.lower() == constants.RESNET101.lower():
        model_ft, criterion, optimizer_ft, exp_lr_scheduler = resnet(class_names, freeze_data_num, learning_rate, 101)
    else:
        model_ft, criterion, optimizer_ft, exp_lr_scheduler = resnet(class_names, freeze_data_num, learning_rate, 34)

    model_to_save = train_model(data_loaders, data_sizes, model_ft, criterion, optimizer_ft, exp_lr_scheduler,
                                num_of_epochs)

    model_name = options.model_to_use \
                 + "_" + options.num_epoch \
                 + "_" + options.batch_size \
                 + "_" + options.learning_rate \
                 + "_" + options.freeze_data_num

    model_pickle_name = model_name + ".pickle"
    model_state_name = model_name + "_model.pt"

    if os.path.exists(model_pickle_name):
        util.writeLog("PICKLE", model_pickle_name + " already exists. Overwriting...")

    if os.path.exists(model_state_name):
        util.writeLog("PICKLE", model_state_name + " already exists. Overwriting...")

    with open(model_pickle_name, 'wb') as handle:
        pickle.dump(model_ft, handle, protocol=pickle.HIGHEST_PROTOCOL)
    # torch.save(model_ft.state_dict(), model_state_name)
