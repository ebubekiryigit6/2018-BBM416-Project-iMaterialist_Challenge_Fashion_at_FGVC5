import json
import os
import urllib.request

import constants
import util


def download_dataset_if_is_necessary():
    json_loader_train = JsonLoader(constants.TRAIN_URLS_JSON_FILE)
    UrlDownloader(json_loader_train.get_url_image_list(), constants.TRAIN_IMAGES_PATH)

    json_loader_test = JsonLoader(constants.TEST_URLS_JSON_FILE)
    UrlDownloader(json_loader_test.url_image_list, constants.TEST_IMAGES_PATH)


class UrlDownloader:
    def __init__(self, url_image_list: list, directory_name):
        util.writeLog("URL Downloader", "Preparing to download images.")
        self.url_image_list = url_image_list
        self.directory_name = directory_name
        self.error_count = 0
        self.__create_directory()
        self.__start_downloading()

    def __create_directory(self):
        # create dataset directory if not exist
        if not os.path.exists(self.directory_name):
            os.makedirs(self.directory_name)
            util.writeLog("URL Downloader", self.directory_name + " is created.")

        # clear previous error files
        if self.directory_name == constants.TRAIN_IMAGES_PATH and os.path.exists(constants.DOWNLOAD_ERRORS_TRAIN_FILE):
            f = open(constants.DOWNLOAD_ERRORS_TRAIN_FILE, 'w')
            f.close()
        if self.directory_name == constants.TEST_IMAGES_PATH and os.path.exists(constants.DOWNLOAD_ERRORS_TEST_FILE):
            f = open(constants.DOWNLOAD_ERRORS_TEST_FILE, 'w')
            f.close()

    def __start_downloading(self):
        util.writeLog("URL Downloader", "Download started.")
        for url_image in self.url_image_list:
            try:
                # if image exist, ignore it
                if not os.path.exists(self.__get_formatted_dir(url_image.get_image_id())):
                    if self.directory_name == constants.TRAIN_IMAGES_PATH:
                        if not os.path.exists(self.__get_alternative_dir(url_image.get_image_id())):
                            urllib.request.urlretrieve(url_image.url,
                                                       self.__get_formatted_dir(url_image.get_image_id()))
                    else:
                        urllib.request.urlretrieve(url_image.url,
                                                   self.__get_formatted_dir(url_image.get_image_id()))
            except:
                # can be broken link or IO Error, write log and error file
                self.error_count += 1
                util.writeLog("Download-Error", "Error in downloading " + url_image.image_id + "   " + url_image.url)
                error_string = url_image.image_id + " " + url_image.url
                if self.directory_name == constants.TRAIN_IMAGES_PATH:
                    self.__write_error_to(constants.DOWNLOAD_ERRORS_TRAIN_FILE, error_string)
                elif self.directory_name == constants.TEST_IMAGES_PATH:
                    self.__write_error_to(constants.DOWNLOAD_ERRORS_TEST_FILE, error_string)
        util.writeLog("URL Downloader", "Download finished with " + str(self.error_count) + " errors.")

    def __get_formatted_dir(self, image_id):
        return self.directory_name + constants.IMAGE_FORMAT.format(image_id)

    def __get_alternative_dir(self, image_id):
        return constants.VAL_IMAGES_PATH + constants.IMAGE_FORMAT.format(image_id)

    def __write_error_to(self, file_name, error_string):
        f = open(file_name, "a")
        f.write(error_string + "\n")
        f.close()


class JsonLoader:
    def __init__(self, file_name):
        self.file_name = file_name
        self.__read_file()

    def __read_file(self):
        # load json objects
        with open(self.file_name) as f:
            data = json.load(f)
            self.data = data
            images = data['images']
            if 'annotations' in data:
                annotations = data['annotations']
            url_image_list = []
            for i in images:
                url_image_list.append(UrlImage(i['url'], i['imageId']))
            annotations_dict = {}
            if 'annotations' in data:
                for j in annotations:
                    annotations_dict[j['imageId']] = j['labelId']
            self.url_image_list = url_image_list
            self.annotations_dict = annotations_dict
        util.writeLog("Json Loader", "Json File is loaded successfully: " + self.file_name)

    def get_data(self):
        return self.data

    def get_url_image_list(self):
        return self.url_image_list

    def get_annotations(self):
        return self.annotations_dict


class UrlImage:
    def __init__(self, url, imageId):
        self.url = url
        self.image_id = imageId

    def get_url(self):
        return self.url

    def get_image_id(self):
        return self.image_id

    def __repr__(self):
        return "ID: {0}, URL: {1}".format(self.image_id, self.url)

    def __str__(self):
        return "ID: {0}, URL: {1}".format(self.image_id, self.url)
