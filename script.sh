#!/usr/bin/env bash

python3.5 ./main.py -m resnet50 -e 10 -b 64 -w 0 -f 150 -l 0.001

python3.5 ./export_csv.py resnet50_10_64_0.001_150.pickle submission.csv


# or


python3.5 ./tensorflow.py 0.001 10