from torch import nn
from torch.autograd import Variable
from torchvision import models
from torchvision.transforms import transforms

RESNET18 = "resnet18"
RESNET34 = "resnet34"
RESNET50 = "resnet50"
RESNET101 = "resnet101"
RESNET152 = "resnet152"


class ResNet:
    __feature = []
    model = None

    def __init__(self, image, resize=224):
        self.image = image
        self.__feature = []
        self.model = RESNET50
        self.resize = resize

    def set_model(self, model=RESNET50):
        self.model = model
        return self

    def execute(self):
        scaler = transforms.Resize((self.resize, self.resize))
        normalizer = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        to_tensor = transforms.ToTensor()
        if self.model.lower() == RESNET18.lower():
            pre_train_model = models.resnet18(pretrained=True)
        elif self.model.lower() == RESNET34.lower():
            pre_train_model = models.resnet34(pretrained=True)
        elif self.model.lower() == RESNET101.lower():
            pre_train_model = models.resnet34(pretrained=True)
        elif self.model.lower() == RESNET152.lower():
            pre_train_model = models.resnet34(pretrained=True)

        else:
            pre_train_model = models.resnet50(pretrained=True)
        pre_train_model = nn.Sequential(*list(pre_train_model.children())[:-1])
        pre_train_model.eval()
        # no train no pain but gain :)
        for p in pre_train_model.parameters():
            p.requires_grad = False
        tensor_img = Variable(normalizer(to_tensor(scaler(self.image))).unsqueeze(0))
        preds = pre_train_model(tensor_img)
        self.__feature = (preds.data.numpy()[0]).flatten()

        # print(self.__feature)
        return self

    def get_feature(self):
        return self.__feature

