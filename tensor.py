import pickle
import sys
import numpy as np
import tensorflow as tf
from PIL import Image
import spatialFeature
import json
import os


# Learning rate and epoch number are taken as arguments.
# From the given json file, uploaded images and labels of these images are saved.
# Using the spatial features and images labels obtained using Resnet50, the Tensorflow model is run.
# In the output, the learning rate, epoch number and accuracy are printed.
def main(argv):
    lr = float(argv[1])
    epoch = int(argv[2])
    print(lr, "  ", epoch)

    trainData = open('project_dataset_json/train_set.json')
    trainData = json.load(trainData)

    featureTrainArray = []

    different = []
    for i in range(len(trainData['annotations'])):
        imageLabels = trainData["annotations"][i]["labelId"]
        for j in range(len(imageLabels)):
            if imageLabels[j] not in different:
                different.append(imageLabels[j])

    differentSize = len(different) + 1
    labelArray = []

    for i in range(len(trainData['annotations'])):
        image = str(trainData["annotations"][i]["imageId"]) + ".jpg"
        image_path = "DATASET/TRAIN/" + image
        if os.path.exists(image_path):
            resnet = spatialFeature.ResNet(Image.open(image_path).convert('RGB')).set_model(
                spatialFeature.RESNET50).execute()
            resnetFeature = resnet.get_feature()
            featureTrainArray.append(resnetFeature)

    size = int(len(featureTrainArray) / 10 * 6)

    for i in range(len(trainData['annotations'])):
        image = str(trainData["annotations"][i]["imageId"]) + ".jpg"
        image_path = "DATASET/TRAIN/" + image
        if os.path.exists(image_path):
            zeros = [0] * differentSize
            labelFeature = trainData["annotations"][i]["labelId"]
            for j in range(len(labelFeature)):
                zeros[different.index(labelFeature[j])] = 1
            labelArray.append(zeros)

    tensorFloww(len(featureTrainArray[0]), differentSize, np.array(featureTrainArray[:size]),
                np.array(labelArray[:size]), np.array(featureTrainArray[size:]), np.array(labelArray[size:]), lr, epoch)


def tensorFloww(inputNumber, maxLabelSize, trainVectors, trainClass, testVectors, testClass, lr, epoch):
    hidden1 = 100  # 1st layer number of neurons
    hidden2 = 100  # 2nd layer number of neurons
    inputNumber = inputNumber  # vector size
    classNumber = maxLabelSize
    X = tf.placeholder("float", [None, inputNumber])
    Y = tf.placeholder("float", [None, classNumber])
    weights = {
        'h1': tf.Variable(tf.random_normal([inputNumber, hidden1])),
        'h2': tf.Variable(tf.random_normal([hidden1, hidden2])),
        'out': tf.Variable(tf.random_normal([hidden2, classNumber]))
    }
    biases = {
        'b1': tf.Variable(tf.random_normal([hidden1])),
        'b2': tf.Variable(tf.random_normal([hidden2])),
        'out': tf.Variable(tf.random_normal([classNumber]))
    }

    layer1 = tf.add(tf.matmul(X, weights['h1']), biases['b1'])
    layer1 = tf.nn.relu(layer1)
    layer2 = tf.add(tf.matmul(layer1, weights['h2']), biases['b2'])
    layer2 = tf.nn.relu(layer2)
    outlayer = tf.matmul(layer2, weights['out']) + biases['out']
    logits = outlayer
    lossOp = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=Y))
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=lr)
    trainOp = optimizer.minimize(lossOp)
    correct_pred = tf.equal(tf.argmax(logits, 1), tf.argmax(Y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
    init = tf.global_variables_initializer()

    with tf.Session() as sess:
        sess.run(init)
        for step in range(epoch):
            sess.run(trainOp, feed_dict={X: trainVectors, Y: trainClass})
            # loss, acc = sess.run([lossOp, accuracy], feed_dict={X: trainVectors, Y: trainClass})
        # Calculate accuracy for test features
        print("Accuracy:", sess.run(accuracy, feed_dict={X: testVectors, Y: testClass}))


if __name__ == "__main__":
    main(sys.argv)
