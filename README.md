# iMaterialist Challenge (Fashion)

BBM416 - Computer Vision

Term Project

### Authors
Saliha Özlem Aydın - 21426677

Ebubekir Yiğit - 21328629



### Project Introduction
In the project, we will try to create a model that will automatically 
solve the problem of classification that occurs in online shopping. 
For this, we first need to interpret the train data. Then we picked 
the right model and made improvements to it. First, we tried to extract 
the spatial features of the images and test it with TensorFlow. 
We thought it could give a lower accuracy but gave more accuracy than we thought. 
We then used ResNet18, ResNet34, ResNet50 and ResNet152 models to fine-tune with CNN. 
We also measured the F1 Score in addition to the accuracy. 
We have increased our Accuracy values. 
We then exported a csv file after completing the prediction with the test data. 
When we submitted this file to Kaggle, our accuracy values ​​did not change much.


## Getting Started

You can browse the "script" files to run the entire process of the project.

For Windows:
```bash
    > script.cmd
```

For Linux:
```bash
    > sh script.sh
```


**All operations are progressively saved to the console.log file. 
You can browse the console.log file to see the operations.**




### Prerequisites

Python 3.5 is required to run the project. 

Python External Libraries:

    PyTorch 0.3.1 - TorchVision - TensorFlow - Numpy - Pillow - OpenCV - Scikit Learn - Scipy
    
If you are having trouble installing PyTorch on Microsoft Windows:

-   Note that your "pip" version is 10.0 and above.
-   Install Anaconda 2 or Anaconda 3
-   Open your Anaconda Prompt as Administrator.
-   Run 'conda create -n vision_env python=3.5 numpy pyyaml mkl' to create your environment
-   Run 'conda activate vision_env' to activate environment
-   Run 'pip install --upgrade pip' to update pip
-   Run 'conda install -c peterjc123 pytorch' to install PyTorch (This command installs latest version of Pytorch if you want to install different version please use pytorch='0.3.1')
-   Run 'pip install docutils pygments pypiwin32 kivy.deps.sdl2 kivy.deps.glew'
-   Run 'pip install kivy.deps.gstreamer'
-   If it doesnt help, please see 'https://www.superdatascience.com/pytorch/'


If you are having trouble installing OPENCV on Microsoft Windows:

-   Note that your "pip" version is 9.0 and above.
-   Open your Anaconda Prompt as Administrator.
-   Run 'conda activate vision_env' to activate environment
-   Run 'pip install opencv-python'


### Running the Project

    python3.5 main.py [options]
    
    Options:
        -m <model_to_use> (resnet18 - resnet34 - resnet50 - resnet101 - resnet152)
        -e <num_of_epoch> (How many epoch?)
        -b <batch_size> (Batch Size of network)
        -w <num_of_workers> (Process number - it depends data size, batch size and epoch size)
        -f <num_of_layers_to_freeze> (How many layers will freeze?)
        -l <learning_rate> (I think optimum is 0.001)
        
    Default Options: (If you dont write any options)
        -m resnet50
        -e 4
        -b 64
        -w 4
        -f 150
        -l 0.001
        and also default GPU Usage: False
        
    python3.5 export_csv.py <pickle_path> <csv_path>
    
to run with TensorFlow:

    python3.5 tensor.py <learning_rate> <num_of_epochs>
    
**If you are running the project for the first time, it will try to download the DataSet. 
To prevent this and json errors**

    DATASETS/
      TRAIN/
      VAL/
      TEST/
      
    project_dataset_json
      test_set.json
      train_set.json

**format the dataset.**


##### WARNING

**The program may experience difficulties downloading some files. 
However, this will write the link and the image id in the train_errors.log or test_errors.log files.**



#### USAGE

To Run with default configs:
```
python3.5 main.py
```
-   Program uses: RESNET50 - 4 Epoch - 64 Batch - 4 Process - Freeze 150 Layers - 0.001 Learning Rate

-   Output: Loss, Train F1 Score, Test F1 Score are shown in console.log file



To Run with your own configs:

    python3.5 main.py -m resnet18 -e 2 -b 32 -w 0 -f 48 -l 0.001
    
-   0 (Zero) worker is only uses main process

-   Output: Loss, Train F1 Score, Test F1 Score are shown in console.log file


To Export .csv file to submission:

    python3.5 export_csv.py resnet18_2_32_0.001_48.pickle submission.csv
    
-   Output: submission.csv file that contains imageId,label format


To Run TensorFlow:
    
    python3.5 tensor.py 0.001 10
    
-   Output: Learning rate, num of epochs and accuracy values.


**After run, please see "console.log" file to show process logs.**

### console.log File
```text
15:20:09   LOG/ MAIN-PROCESS:  Image Classification of Fashion Products is started.
15:20:09   LOG/ ARG-PARSER:  Argument Informations:
                             MODEL: resnet50
                             EPOCH: 8
                             BATCH SIZE: 64
                             WORKERS: 0
                             LEARNING RATE: 0.001
                             FREEZE LAYER NUM: 141
15:20:09   LOG/ GPU-ENGINE:  GPU Enabled: False
15:20:09   LOG/ MODEL-FREEZER:  Freeze Model Parameters: 141
15:20:09   LOG/ JSON LOADER:  Json File is loaded successfully: project_dataset_json/train_set.json
15:20:10   LOG/ JSON LOADER:  Json File is loaded successfully: project_dataset_json/train_set.json
15:20:10   LOG/ DATA-LOADER:  Train and Test Data has been loaded.
15:20:12   LOG/ TRAINING:  Train is started with
                           Train Size: 14967
                           Test Size:  10000
15:20:12   LOG/ EPOCH-LOGGER:  Epoch 0/7
16:48:01   LOG/ F1-SCORE:  Epoch 0/7 Score: 86.41093537021986
16:48:01   LOG/ LOSS-VALUE:  Epoch 0/7 Loss: 0.07655451908182936
17:40:52   LOG/ F1-SCORE:  Epoch 0/7 Score: 65.77989228425083
17:40:52   LOG/ LOSS-VALUE:  Epoch 0/7 Loss: 0.06970662677288056
17:40:52   LOG/ EPOCH-LOGGER:  Epoch 1/7
19:22:39   LOG/ F1-SCORE:  Epoch 1/7 Score: 103.27530259116602
19:22:39   LOG/ LOSS-VALUE:  Epoch 1/7 Loss: 0.06323462754703041
20:14:23   LOG/ F1-SCORE:  Epoch 1/7 Score: 68.21935541185252
20:14:23   LOG/ LOSS-VALUE:  Epoch 1/7 Loss: 0.0664537279844284
20:14:23   LOG/ EPOCH-LOGGER:  Epoch 2/7
21:46:53   LOG/ F1-SCORE:  Epoch 2/7 Score: 113.0811380382309
21:46:53   LOG/ LOSS-VALUE:  Epoch 2/7 Loss: 0.058669986055092764
00:48:39   LOG/ F1-SCORE:  Epoch 2/7 Score: 69.46062589170057
00:48:39   LOG/ LOSS-VALUE:  Epoch 2/7 Loss: 0.06599582280516625
00:48:39   LOG/ EPOCH-LOGGER:  Epoch 3/7
02:35:20   LOG/ F1-SCORE:  Epoch 3/7 Score: 123.37022091303375
02:35:20   LOG/ LOSS-VALUE:  Epoch 3/7 Loss: 0.05386456927336743
03:27:51   LOG/ F1-SCORE:  Epoch 3/7 Score: 70.79014542786628
03:27:51   LOG/ LOSS-VALUE:  Epoch 3/7 Loss: 0.06711776089668274
03:27:51   LOG/ EPOCH-LOGGER:  Epoch 4/7
05:08:08   LOG/ F1-SCORE:  Epoch 4/7 Score: 134.70361619531218
05:08:08   LOG/ LOSS-VALUE:  Epoch 4/7 Loss: 0.04860288591878347
06:00:13   LOG/ F1-SCORE:  Epoch 4/7 Score: 69.29540404475343
06:00:13   LOG/ LOSS-VALUE:  Epoch 4/7 Loss: 0.06904236804246902
06:00:13   LOG/ EPOCH-LOGGER:  Epoch 5/7
07:36:33   LOG/ F1-SCORE:  Epoch 5/7 Score: 148.62407788431145
07:36:33   LOG/ LOSS-VALUE:  Epoch 5/7 Loss: 0.042417815736288805
08:28:13   LOG/ F1-SCORE:  Epoch 5/7 Score: 72.85843668334057
08:28:13   LOG/ LOSS-VALUE:  Epoch 5/7 Loss: 0.07264988178014756
08:28:13   LOG/ EPOCH-LOGGER:  Epoch 6/7
10:04:45   LOG/ F1-SCORE:  Epoch 6/7 Score: 163.18661390184357
10:04:45   LOG/ LOSS-VALUE:  Epoch 6/7 Loss: 0.03589061663035616
10:56:37   LOG/ F1-SCORE:  Epoch 6/7 Score: 71.05219581598739
10:56:37   LOG/ LOSS-VALUE:  Epoch 6/7 Loss: 0.07791561430692673
10:56:37   LOG/ EPOCH-LOGGER:  Epoch 7/7
13:54:46   LOG/ F1-SCORE:  Epoch 7/7 Score: 183.58234964510373
13:54:46   LOG/ LOSS-VALUE:  Epoch 7/7 Loss: 0.026154986120426417
14:56:48   LOG/ F1-SCORE:  Epoch 7/7 Score: 73.3905873807523
14:56:48   LOG/ LOSS-VALUE:  Epoch 7/7 Loss: 0.07328572632074357
14:56:48   LOG/ TRAINING:  Training complete in 1416m 36s
14:56:48   LOG/ TRAINING:  Best val Acc: 73.390587
14:56:57   LOG/ MAIN-PROCESS:  Image Classification of Fashion Products is finished in 85007.64515972137 seconds.
```
**We forgot to separate train and validation F1 Scores. Sorry for that. This is why F1 score is bigger than 1.**